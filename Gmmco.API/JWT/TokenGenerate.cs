﻿using Gmmco.API.Installer.Settings;
using Gmmco.Data.BusinessModel;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Gmmco.API.JWT
{
    public class TokenGenerate
    {
        private readonly JWTSetting _JWTSetting;
        public TokenGenerate(JWTSetting jWTSetting)
        {
            _JWTSetting = jWTSetting;
        }
        public string Token(UserLoginModel user)
        {
            var Tokenhandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_JWTSetting.JWTSecrateKey);
            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(type: JwtRegisteredClaimNames.Sub, value: user.UserName),
                    new Claim(type: JwtRegisteredClaimNames.Jti, value: Guid.NewGuid().ToString()),
                    new Claim(type: "id", value: Convert.ToString(user.UserId)),
                }),
                Expires = DateTime.UtcNow.Add(_JWTSetting.JWTTokenLifeTime),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), algorithm: SecurityAlgorithms.HmacSha256Signature),
            };
            var token = Tokenhandler.CreateToken(tokenDescription);
            return Tokenhandler.WriteToken(token);
        }
    }
}
