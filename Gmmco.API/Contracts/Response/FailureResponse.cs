﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gmmco.API.Contracts.Response
{
    public class FailureResponse<T>
    {
        public string Error { get; set; }
        public bool IsreponseSuccess { get; set; } = false;
    }
}
