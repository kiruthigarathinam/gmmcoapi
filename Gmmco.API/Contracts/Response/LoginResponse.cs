﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gmmco.API.Contracts.Response
{
    public class LoginResponse<T>
    {
        public string Error { get; set; }
        public bool ISsuccessResponse { get; set; } = false;
        public string Token { get; set; }
        public string Response { get; set; }
    }
}
