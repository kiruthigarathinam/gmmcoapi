﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gmmco.API.Contracts.APIRoutes
{
    public class APIRoutes
    {
        public const string Version = "V1";
        public const string Root = "api";
        public const string Base = Root + "/" + Version;
        public static class Accounts
        {
            public const string Login = Base + "/Account/Login";
        }
    }
}
