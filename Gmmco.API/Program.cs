using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gmmco.Data.DB_Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Gmmco.API
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
           var Host = CreateHostBuilder(args).Build();

            using (var Service = Host.Services.CreateScope())
            {
                var Db = Service.ServiceProvider.GetRequiredService<GmmcoContext>();
                //await Db.Database.MigrateAsync();
            }
            await Host.RunAsync();
           
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureLogging((hostingContext, logging) => {
                logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                logging.AddConsole();
                logging.AddDebug();
                logging.AddEventSourceLogger();
                logging.ClearProviders();
                logging.AddEventLog();
            })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
