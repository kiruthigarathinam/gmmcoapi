﻿using System;
using System.Threading.Tasks;
using Gmmco.API.Contracts.APIRoutes;
using Gmmco.API.Contracts.Response;
using Gmmco.API.JWT;
using Gmmco.Data.BusinessModel;
using Gmmco.Repository.UnitOfWork;
using Gmmco.Service.Interface.IUser;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Gmmco.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly TokenGenerate token;
        private readonly ILogger<AccountController> logger;
        private readonly IUnitofWork Uow;
        private readonly IConfiguration config;

        public AccountController(IUserService _userService, TokenGenerate _token,
           IUnitofWork _Uow, IConfiguration _config, ILogger<AccountController> _logger)
        {
            userService = _userService;
            token = _token;
            Uow = _Uow;
            config = _config;
            logger = _logger;

        }

        [HttpPost(APIRoutes.Accounts.Login)]

        public async Task<IActionResult> Login(LoginModel LoginModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    logger.LogInformation("Username and Password is required");
                    return Ok(new LoginResponse<object>()
                    {
                        Error = "Please enter User Name / Password",
                        ISsuccessResponse = false
                    });
                }
                else
                {
                    UserLoginModel user = new UserLoginModel();
                    user = userService.Login(LoginModel);

                    if (user.Response)
                    {
                        return Ok(new LoginResponse<UserLoginModel>()
                        {
                            ISsuccessResponse = true,
                            Response = "Successfully Logged In",
                            Token = token.Token(user),
                        });
                    }

                    else
                    {
                        logger.LogInformation("Provided credentials is invalid");
                        return Unauthorized(new LoginResponse<object>()
                        {
                            Error = user.Error,
                        });
                    }

                }
            }
            catch (Exception ex)
            {
                return Ok(new FailureResponse<object>()
                {
                    
                    Error = "Failure",
                    IsreponseSuccess = false
                });
            }
        }
    }
}