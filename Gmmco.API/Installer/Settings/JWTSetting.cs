﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gmmco.API.Installer.Settings
{
    public class JWTSetting
    {
        public string JWTSecrateKey { get; set; }
        public TimeSpan JWTTokenLifeTime { get; set; }
    }
}
