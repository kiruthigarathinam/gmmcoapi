﻿using Gmmco.Data.DB_Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Gmmco.API.Installer.Settings;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;
using Gmmco.Repository.UnitOfWork;
using Gmmco.Service.Interface.IUser;
using Gmmco.Service.Implementation.UserService;
using Gmmco.API.JWT;

namespace Gmmco.API.Installer
{
    public class IServiceCollectionExtention
    {
        /// <summary>
        /// Db Installer
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection DbInstraller(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContextPool<GmmcoContext>(options =>
            options.UseSqlServer(configuration.GetValue<string>("ConnectionStrings:gmmcoDev"),
            Migration => Migration.MigrationsAssembly("Gmmco.Data")));
            return services;
        }

        /// <summary>
        /// JWT Token Middle ware
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection JWTHandlerEnable(IServiceCollection services, IConfiguration configuration)
        {
            var JWTSetting = new JWTSetting();
            configuration.Bind(key: nameof(JWTSetting), JWTSetting);
            services.AddSingleton(JWTSetting);

            var TokenHandlerParameter = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key: Encoding.ASCII.GetBytes(JWTSetting.JWTSecrateKey)),
                ValidateIssuer = false,
                ValidateAudience = false,
                RequireExpirationTime = false,
                ValidateLifetime = true,
            };

            services.AddSingleton(TokenHandlerParameter);

            services.AddAuthentication(configureOptions: x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.SaveToken = true;
                x.TokenValidationParameters = TokenHandlerParameter;

            });

            return services;
        }

        /// <summary>
        /// swagger enabing
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection SwaggerEnable(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Gmmco_API", Version = "v1" });

                options.AddSecurityDefinition(name: "Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the bearer scheme",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement {

                    {new OpenApiSecurityScheme{

                        Reference = new OpenApiReference
                        {
                            Id="Bearer",
                            Type = ReferenceType.SecurityScheme
                        }

                    },new List<string>()}

                });
            });
            return services;
        }

        /// <summary>
        /// Depency Injection
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection Dependencies(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<GmmcoContext>();
            services.AddScoped<IUnitofWork, UnitofWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<TokenGenerate>();
            return services;
        }

    }
}
