﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gmmco.Repository.UnitOfWork
{
    public interface IUnitofWork
    {
        IRepository<T> Repository<T>() where T : class;
        void SaveChanges();
        void Dispose();

    }
}
