﻿using Gmmco.Data.DB_Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gmmco.Repository.UnitOfWork
{
   public class UnitofWork : IUnitofWork
    {
        private readonly GmmcoContext gmmcoContext;
        public Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        public UnitofWork(GmmcoContext _gmmcoContext)
        {
            gmmcoContext = _gmmcoContext;
        }


        public IRepository<T> Repository<T>() where T : class
        {
            if (repositories.Keys.Contains(typeof(T)) == true)
            {
                return repositories[typeof(T)] as IRepository<T>;
            }
            IRepository<T> repo = new GenericRepository<T>(gmmcoContext);
            repositories.Add(typeof(T), repo);
            return repo;
        }

        public void SaveChanges()
        {
            gmmcoContext.SaveChanges();
        }

        public void Dispose()
        {
            gmmcoContext.Dispose();
        }
    }
}
