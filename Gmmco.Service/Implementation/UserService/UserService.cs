﻿using Gmmco.Data.BusinessModel;
using Gmmco.Data.Tables;
using Gmmco.Repository.UnitOfWork;
using Gmmco.Service.Interface.IUser;
using Gmmco.Service.SecurityScheme;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gmmco.Service.Implementation.UserService
{
    public class UserService : IUserService
    {
        private IConfiguration configuration;
        private readonly IUnitofWork unitofWork;     
        public UserService(IUnitofWork _unitofWork, IConfiguration _configuration)
        {
            unitofWork = _unitofWork;
            configuration = _configuration;
        }
        public UserLoginModel Login(LoginModel loginmodel)
        {
            try
            {
                string SecarteKey = configuration.GetValue<string>("SecrateKey");
                UserLoginModel Usermodel = new UserLoginModel();
                var EmailCheck = unitofWork.Repository<User_Tbl>().GetFirstOrDefault(u => u.UserName == loginmodel.UserName);
                if (EmailCheck != null)
                {
                    var passwordcheck = unitofWork.Repository<User_Tbl>().GetFirstOrDefault(u => u.UserName == loginmodel.UserName && u.Password == loginmodel.Password);

                    if (passwordcheck != null)
                    {

                        Usermodel = new UserLoginModel()
                        {
                            UserId = passwordcheck.UserId,
                            UserName = passwordcheck.UserName,
                            SecerateCodeForNewToken = passwordcheck.Password

                        };
                    }
                    else
                    {

                        Usermodel = new UserLoginModel()
                        {
                            Response = false,
                            Error = "Entered Password is wrong"
                        };
                    }
                }

                else
                {
                    Usermodel = new UserLoginModel()
                    {
                        Response = false,
                        Error = "User Email Does Not Exists"
                    };
                }
                return Usermodel;
            } catch(Exception ex)
            {
                throw ex;
            }
            
        }
    }
}
