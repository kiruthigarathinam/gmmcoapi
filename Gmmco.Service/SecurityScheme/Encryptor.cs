﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Gmmco.Service.SecurityScheme
{
   public static class Encryptor
    {
        private static byte[] key = { };
        private static readonly byte[] IV = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
        private static byte[] inputByteArray = { };

        public static string EncryptData(string data, string SecrateKey)
        {
            try
            {
                SecrateKey = Base64Decode(SecrateKey);
                key = null;
                key = Encoding.UTF8.GetBytes(SecrateKey);
                DESCryptoServiceProvider objDESC = new DESCryptoServiceProvider();
                inputByteArray = null;
                inputByteArray = Encoding.UTF8.GetBytes(data);
                MemoryStream Objmst = new MemoryStream();
                CryptoStream Objcs = new CryptoStream(Objmst, objDESC.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                Objcs.Write(inputByteArray, 0, inputByteArray.Length);
                Objcs.FlushFinalBlock();
                return Convert.ToBase64String(Objmst.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
