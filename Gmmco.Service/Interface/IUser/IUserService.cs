﻿using Gmmco.Data.BusinessModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gmmco.Service.Interface.IUser
{
    public interface IUserService
    {
        UserLoginModel Login(LoginModel loginmodel);
    }
}
