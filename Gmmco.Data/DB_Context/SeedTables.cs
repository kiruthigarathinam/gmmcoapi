﻿using Gmmco.Data.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gmmco.Data.DB_Context
{
    public static class SeedTables
    {
        public static void seedMasterTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User_Tbl>(entity =>
            {
                entity.ToTable("User_Tbl");

                entity.HasKey(e => e.UserId)
                  .HasName("PRIMARY_UserId");

                entity.Property(e => e.UserId).HasColumnName("UserID");
                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
                entity.Property(e => e.Password)
                     .HasMaxLength(45)
                     .IsUnicode(false);
            });
        }
    }
}
