﻿using Gmmco.Data.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gmmco.Data.DB_Context
{
   public class GmmcoContext: DbContext
    {
        public GmmcoContext(DbContextOptions<GmmcoContext> dboptions) : base(dboptions)
        {

        }
        public virtual DbSet<User_Tbl> User_Tbls { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SeedTables.seedMasterTable(modelBuilder);
        }
    }
}
