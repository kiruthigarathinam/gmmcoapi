﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Gmmco.Data.BusinessModel
{
    public class LoginModel
    {
        [Required(ErrorMessage = "User Name is Required")]
        [StringLength(100)]
        public string UserName { get; set; }
        [Required(ErrorMessage = "password is Required")]
        [DataType(DataType.Password)]
        [StringLength(30)]
        public string Password { get; set; }
    }
}
