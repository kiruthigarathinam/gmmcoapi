﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Gmmco.Data.BusinessModel
{
   public class UserModel
    {
    }
    public class UserLoginModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string SecerateCodeForNewToken { get; set; }

        [JsonIgnore]
        public bool Response { get; set; } = true;
        [JsonIgnore]
        public string Error { get; set; } = "Success";
    }
}
